This is an interpreter for the language kernel. 
# how it works
  You enclose kernel code in the macro 
```
(kernel 
  ($define! $lambda  
	    ($vau (ptree . body)
		  static-env
		  (wrap (eval (list* $vau ptree _ body) static-env)))))
```
  When this code is executed, the interpreter (`keval`) will evaluate 		  
  the args to `kernel` in the null environment.
# spec fail
  currently none of the language but the core primitives are implemented,
  and these ar not even implemented correctly according to the (un)revised report 
  on the kernel language: http://web.cs.wpi.edu/~jshutt/kernel.html
## environments
   For ease of implementation, so far environments are just singly-linked lists. 
   A child environment is merely a series of bindings cons'd onto its parents.
   Hence, your environment can only have one parent.
## `$define!`
   this works like `define` in scheme by ammending the toplevel, not $define!
   as in the kernel specs. Again, ease of implementation over correctness.
## `call/cc`
   I haven't implemented this at all. 
