;;;; kernel.asd

(asdf:defsystem #:kernel
  :description "Describe kernel here"
  :author "Your Name <your.name@example.com>"
  :license "Specify license here"
  :depends-on (#:optima
               #:iterate
               #:fare-quasiquote
	       #:fare-quasiquote-extras
               #:fare-quasiquote-optima
               #:alexandria)
  :serial t
  :components ((:file "kernel")))

