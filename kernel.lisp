;;;; kernel.lisp
(defpackage #:kernel
  (:use #:cl #:optima #:iterate #:alexandria))
(in-package #:kernel)

(fare-quasiquote:enable-quasiquote)
(defmacro aif (test true &rest false)
  `(let ((it ,test))
     (if it ,true ,@false)))

(defun partial (func &rest args1)
  (lambda (&rest args2)
    (apply func (append args1 args2))))
 

;;; combiners will be represented  as
;;; ($combiner wrap-depth fexpr lex-env) 

;;; env: (list alist-bindings list-of-parents)

(defvar *unbound* (gensym "unbound"))
(defparameter *ground* (make-hash-table :test 'eq))

(defun lookup (env symb)
  (aif (assoc symb env)
       (cdr it)
       (gethash symb *ground* *unbound*)))

(defun match-bind (argtree args &optional bindings)
  (match (list argtree args)
    ((list nil nil) bindings)
    ((list '_ _) bindings)
    ((list (type symbol) _) (cons (cons argtree args)
					  bindings))
    ((list (cons tcar tcdr ) (cons acar acdr ))
     (match-bind tcdr acdr (match-bind tcar acar bindings)))
    (_ (error "bad bindings"))))

(defun extend (env bindings)
  (append bindings env))

(defun kcall (dyn-env f arg)
  (match f
    (`($combiner 0 ($vau ,argtree ,dyn-name . ,body) ,lex-env)
      (keval
       (extend lex-env
	       (match-bind (list dyn-name argtree)
			   (list dyn-env arg)))
       (cons 'progn body)))
    ((guard `($combiner 0 ,fn) (functionp fn))
     (funcall fn dyn-env arg))
    ((guard `($combiner ,n . ,stuff) (> n 0))
      (kcall dyn-env `($combiner ,(1- n) . ,stuff)
	     (mapcar (partial #'keval dyn-env) arg)))
    (_ (error "malformed combiner"))))
  
(defun kwrap (env arg)
  (match (car arg) 		;applicator
    (`($combiner ,n ,vau-exp ,lex-env)
      `($combiner ,(1+ n) ,vau-exp ,lex-env))
    (it (error "malformed wrapped combiner ~a" it))))

(defun kunwrap (env arg)
  (match (car arg) 		;applicator
    ((guard `($combiner ,n ,vau-exp ,lex-env) (> n 0))
      `($combiner ,(1- n) ,vau-exp ,lex-env))
    (_ (error "malformed wrapped combiner"))))

(defun kdefine (env arg)
  (match arg
    ((list name val)
     (if (symbolp name)
	 (setf (gethash name *ground*) (keval env val))
	 (error "name must be symbol")))))

(defun kkeval (env arg)
  (declare (ignore env))
  (match arg
    ((list exp var-env)
     (keval var-env exp))))

(defun self-evalp (exp)
  (match exp
    ((or
      '_
      '$vau '$combiner 
      (type keyword) 
      t nil
;      `($compiled-combiner . ,_)
      `($combiner . ,_))
     t)
    ((or (type symbol) (type list)) nil)
    (_ t)))

(defun keval (env exp)
  (match exp
    ((eq *unbound*) (error "unbound"))
    ((satisfies self-evalp)  exp)
    (`($if ,test ,true ,false)
      (if (keval env test)
	  (keval env true)
	  (keval env false)))
    (`($vau ,_ ,_ . ,_)
      `($combiner 0 ,exp ,env))
    (`(progn ,x) (keval env x))
    (`(progn ,x . ,y)
      (keval env x)
      (keval env (cons 'progn y)))
    ((guard `(,f . ,arg) (self-evalp f)) (kcall env f arg))
    (`(,f . ,arg) (keval env (cons (keval env f) arg)))
    ((type symbol) (lookup env exp))
    (_ exp)))

(kcall nil `($combiner 0 ($vau nil _ :womp) (list nil nil)) nil) 

(defmacro genf (fun)
  (with-gensyms (env args)
    `(lambda (,env ,args)
       (declare (ignore ,env))
       (apply #',fun ,args))))

(defmacro name-kfuns (&rest name-vals)
  `(setf
    ,@(iter (for (name wrap val . rest) on  name-vals by #'cdddr)
	    (appending
	     `((gethash ',name *ground*)
	       `($combiner ,,wrap
			   ,,(if (symbolp val)
				 `#',val
				 val)))))))

(defmacro auto-import-kfuns (&rest names)
  `(name-kfuns 
    ,@(iter (for name in names)
	    (appending `(,name 1 (genf ,name))))))

(defmacro kernel (&body body)
  `(keval nil '(progn ,@body)))

(name-kfuns
 wrap 1 kwrap
 unwrap 1 kunwrap 
 $define! 0 kdefine
 eval 1 kkeval)


(auto-import-kfuns
 identity
 list list* cons
 car cdr cadr cddr
 eq eql equal
 mapcar
 + - * / < > = <= >=)

(kernel
  ($define! list
	    (wrap ($vau x _ x)))

  ($define! get-cur-env
	    ($vau () env env))

  ($define! $lambda  
	    ($vau (ptree . body)
		  static-env
		  (wrap (eval (list* $vau ptree _ body) static-env))))

  ($define! $aif 
	    ($vau (test true false) env
		  (($lambda (it)
		       ($if it
			    (eval true (get-cur-env))
			    (eval false (get-cur-env))))
		   test)))
  ($define! map1
	    ($lambda (fn list)
		($if list
		     (cons
		      (fn (car list))
		      (map1 fn (cdr list)))
		     nil)))
  ($define! $let
	    ($vau (bindings . body) here
		  (eval 
		   (cons
		    (list* $lambda (map1 car bindings) body)
		    (map1 cadr bindings))
		   here))))

